const { expect } = require("chai");

describe("MyContract", function() {
  it("should set and get the message", async function() {
    const MyContract = await ethers.getContractFactory("MyContract");
    const myContract = await MyContract.deploy();

    await myContract.setMessage("Hello, world!");

    expect(await myContract.getMessage()).to.equal("Hello, world!");
  });
});
