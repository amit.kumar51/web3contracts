require("@nomicfoundation/hardhat-toolbox");
require("dotenv").config();
require("hardhat-contract-sizer");

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: "0.8.18",
  networks: {
    sepolia: {
      url: `https://mainnet.infura.io/v3/${process.env.SEPOLIA_URL}`,
      accounts: process.env.WALLET_KEY,
    },
  },
  etherscan: {
    apiKey: process.env.ETHSCAN_API,
  },
};
