// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract MyToken is ERC20 {
    mapping(address => bool) public hasMinted;

    constructor(uint256 initialSupply) ERC20("MyToken", "MTK") {
        _mint(msg.sender, initialSupply);
    }

    function mint(uint256 amount) public {
        require(!hasMinted[msg.sender], "You can only mint once.");
        _mint(msg.sender, amount);
        hasMinted[msg.sender] = true;
    }

    function burn(uint256 amount) public {
        _burn(msg.sender, amount);
    }

    function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual override {
        require(false, "Transfers are not allowed.");
        super._beforeTokenTransfer(from, to, amount);
    }
}