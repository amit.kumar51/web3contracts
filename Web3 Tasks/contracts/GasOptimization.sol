// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17; 

//Optimization Technique	           Optimized		UnOptimized	
//Call data	                              61528		        64548	
//load state variables to memory	      60530		        61528	
//loop increments	                      55533		        60530	
//uncheck i overflow/underflow	          53440		        55533	 

// gas golf
contract GasGolf {
    // start - ? gas
    // use calldata - ? gas
    // load state variables to memory - ? gas
    // short circuit - ? gas
    // loop increments - ? gas
    // cache array length - ? gas
    // load array elements to memory - ? gas
    // uncheck i overflow/underflow - ? gas

    uint256 public total;


    // Test Subject
    function sumIfEvenAndLessThan99Test(uint256[] calldata nums) external {
        uint256 totalLocal = total;

        for (uint256 i = 0; i < nums.length; ) {
            bool isLessThan99 = nums[i] < 99;

            unchecked {
                if (isLessThan99) totalLocal += nums[i];
                i += 2;
            }
        }

        total = totalLocal;
    }

}
