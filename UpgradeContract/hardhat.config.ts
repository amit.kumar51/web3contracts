import { HardhatUserConfig } from "hardhat/config";
import { ethers } from "hardhat";
import "@nomiclabs/hardhat-etherscan";
import "@nomicfoundation/hardhat-toolbox";
import "@openzeppelin/hardhat-upgrades";
import dotenv from "dotenv";

dotenv.config();

const config: HardhatUserConfig | any = {
  solidity: "0.8.18",
  networks: {
    sepolia: {
      url: `${process.env.SEPOLIA_URL}`,
      accounts: [process.env.WALLET_KEY],
    },
  },
  etherscan: {
    apiKey: process.env.ETHSCAN_API,
  },
};

export default config;
