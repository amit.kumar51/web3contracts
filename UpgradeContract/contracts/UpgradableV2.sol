// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17; 

import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "./UpgradableV1.sol";

contract MyTokenV2 is MyToken {

    function name() public pure override returns (string memory) {
        return "Hii My name is Amit";
    }


}