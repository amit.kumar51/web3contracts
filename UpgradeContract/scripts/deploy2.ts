import { ethers, upgrades, run } from "hardhat";

const PROXY_ADDRESS: string | any = process.env.PROXY_ADDRESS;

async function main() {
  //Deploying implementation
  const MyTokenV2: any = await ethers.getContractFactory("MyTokenV2");

  const upgrade = await upgrades.upgradeProxy(PROXY_ADDRESS, MyTokenV2);
  //console.log("Contract address: ", upgrade.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
