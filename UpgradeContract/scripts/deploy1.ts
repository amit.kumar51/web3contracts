import { ethers, upgrades } from "hardhat";
async function main() {
  //Deploying implementation
  const MyToken = await ethers.getContractFactory("MyToken");

  const deployProxy = await upgrades.deployProxy(MyToken, {
    kind: "uups",
  });
  console.log("Contract address: ", deployProxy.address);
  await deployProxy.deployed();
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
